jQuery(document).ready(function () {
  new WOW().init();

  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const amount = urlParams.get('amount')
  const click_id = urlParams.get('click_id') || Math.floor(1000000 + Math.random() * 900000) + '' + Math.floor(1000000 + Math.random() * 900000) + '' + Math.floor(1000000 + Math.random() * 900000) + '' + Math.floor(1000000 + Math.random() * 900000)
  // const click_id = Math.floor(1000000 + Math.random() * 900000) + '' + Math.floor(1000000 + Math.random() * 900000) + '' + Math.floor(1000000 + Math.random() * 900000) + '' + Math.floor(1000000 + Math.random() * 900000)

  // var img = document.getElementById('pixel');
  // img && img.insertAdjacentHTML('afterbegin', `<img src="https://track.cpamrkt.kz/track/goal-by-click-id?click_id=${click_id}&amount=${amount}&track_id=${amount}" height="1" width="1" style="position: absolute" />`);

  // Scrolling 
  const anchors = document.querySelectorAll('a[href*="#"]')
  for (let anchor of anchors) {
    anchor.addEventListener('click', function (e) {
      e.preventDefault();

      const blockID = anchor.getAttribute('href')
      document.querySelector(blockID).scrollIntoView({
        behavior: 'smooth',
        block: 'start'
      })
    })
  }

  $('input[name=phone]').inputmask({
    "mask": "+7 (999) 999-99-99"
  }); //specifying options
  $('input[name=iin]').inputmask({
    "mask": "999999999999",
    clearIncomplete: !0
  }); //specifying options
  $("input[name=sum]").inputmask("integer", { rightAlign: !1, groupSeparator: " ", groupSize: 3, placeholder: "", max: 100000 })
  $("input[name=period]").inputmask("integer", { rightAlign: !1, placeholder: "", max: 36 })


  const btn = document.querySelector("button.mobile-menu-button");
  const menu = document.querySelector(".mobile-menu");

  btn.addEventListener("click", () => {
    menu.classList.toggle("hidden");
  });
});

// Modal open modal
const modal_overlay = document.querySelector('#modal_overlay');
function openModal(modal_id, value) {
  const modal = document.querySelector(modal_id);
  const modalCl = modal.classList
  const overlayCl = modal.closest('.modal_overlay');

  if (value) {
    overlayCl.classList.remove('hidden')
    setTimeout(() => {
      modalCl.remove('opacity-0')
      modalCl.remove('-translate-y-full')
      modalCl.remove('scale-150')
      modalCl.remove('hidden')
    }, 100);
  } else {
    modalCl.add('-translate-y-full')
    setTimeout(() => {
      modalCl.add('opacity-0')
      modalCl.add('scale-150')
      modalCl.add('hidden')
    }, 100);
    setTimeout(() => overlayCl.classList.add('hidden'), 300);
  }
}
