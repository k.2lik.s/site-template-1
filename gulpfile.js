'use strict';
let gulp = require('gulp'),
    pug = require('gulp-pug'),
    sass = require('gulp-sass')(require('sass')),
    minify = require('gulp-minify');

gulp.task('js', function () {
    return gulp.src('app/js/*.js')
        .pipe(minify({
            ext: { min: '.min.js' },
            ignoreFiles: ['*.min.js']
        }))
        .pipe(gulp.dest('app/mjs'))
});

gulp.task('pug', function () {
    return gulp.src('app/*.pug')
        .pipe(pug({
            pretty: true
        }))
        .pipe(gulp.dest('app'))
});

gulp.task('sass', function () {
    return gulp.src('app/sass/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('app/css'));
});



gulp.task('watch', function () {
    gulp.watch('app/js/*.js', gulp.parallel('js'));
    gulp.watch('app/*.pug', gulp.parallel('pug'));
    gulp.watch('app/sass/*.scss', gulp.parallel('sass'));
});

gulp.task('default', gulp.parallel('watch'));